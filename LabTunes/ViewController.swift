//
//  ViewController.swift
//  LabTunes
//
//  Created by Saul G on 11/9/18.
//  Copyright © 2018 SaulG. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBAction func loginButtonTouchUpInside(_ sender: Any) {
        
        guard let username = userNameTextField.text else { return }
        guard let password = passwordTextField.text else { return }
        
        if User.login(userName: username, password: password){
            performSegue(withIdentifier: "loginSuccess", sender: self)
        }
        else {
            return
        }
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }


}

