//
//  LabTunesTests.swift
//  LabTunesTests
//
//  Created by Saul G on 11/9/18.
//  Copyright © 2018 SaulG. All rights reserved.
//


import XCTest
@testable import LabTunes

class LabTunesTests: XCTestCase {
    
    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        let session = Session.sharedInstance
        session.token = nil
    }
    
    func testCorrectLogin(){
        XCTAssertTrue(User.login(userName: "iOSLab", password: "Prueba"))
        XCTAssertFalse(User.login(userName: "MOXith0p", password: "Prueba"))
    }
    
    func testSaveSession() {
        let session = Session.sharedInstance
        let _ = User.login(userName: "iOSLab", password: "password")
        XCTAssertNotNil(session.token)
    }
    
    func testUnsavedSession() {
        let session = Session.sharedInstance
        let _ = User.login(userName: "iOSLxksghab", password: "password")
        XCTAssertNil(session.token)
    }
    
    func testExpectedToken() {
        let _ = User.login(userName: "iOSLab", password: "password")
        let session = Session.sharedInstance
        XCTAssertEqual(session.token!, "123456789", "Token should Match")
        XCTAssertNotEqual(session.token!, "djknvsjkn", "Token matches")
    }
    
    func testFetchSongsThrowsError() {
        XCTAssertThrowsError(try User.fetchSongs())
    }
    
    func testMusicSongs() {
        var resultSongs: [Song] = []
        let promise = expectation(description: "Songs Downloades")
        Music.fetchSongs {(songs) in
            resultSongs = songs
            promise.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNotEqual(resultSongs.count, 0)
    }
    
    
    
}
